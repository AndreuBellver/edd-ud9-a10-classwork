package com.edd.test;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;


public class TestAlarmWithoutMock {

    /**
     * Comprobar que la alarma está desactivada por defecto
     */
    @Test
    public void testAlarmIsNotOnByDefault() {

        Alarm alarm = new Alarm(new Sensor());
        Assertions.assertFalse(alarm.isAlarmOn(), "Alarm is not off by default");

    }
    /**
     * Comprobar que la alarma se activa en con bajas presiones
     */
    @Test
    public void testAlarmOnWithLowPressure() {

        Sensor sensor = new Sensor();
        Alarm alarm = new Alarm(sensor);

        alarm.check(Alarm.LOW_PRESSURE_THRESHOLD -1);

        assertTrue(alarm.isAlarmOn());
    }

    /**
     * Comprobar que la alarma se activa en con altas presiones
     */
    @Test
    public void testAlarmOnWithHighPressure() {

        Sensor sensor = new Sensor();
        Alarm alarm = new Alarm(sensor);

        alarm.check(Alarm.HIGH_PRESSURE_THRESHOLD + 1);

        assertTrue(alarm.isAlarmOn());
    }


    /**
     * Comprobar que la alarma permanece desactivda con presiones normales
     */
    @Test
    public void testAlarmOffWithNormalPressure() {

        Sensor sensor = new Sensor();
        Alarm alarm = new Alarm(sensor);

        alarm.check(Alarm.HIGH_PRESSURE_THRESHOLD -2);

        assertFalse(alarm.isAlarmOn());

    }

    /**
     * Comprobar que la alarma permanece desactivda con presiones límite
     */
    @ParameterizedTest
    @ValueSource(doubles = {Alarm.HIGH_PRESSURE_THRESHOLD - 1,Alarm.LOW_PRESSURE_THRESHOLD + 1})
    public void testAlarmOffWithLimitsPressure(double preasure) {

        Sensor sensor = new Sensor();
        Alarm alarm = new Alarm(sensor);

        alarm.check(preasure);

        assertFalse(alarm.isAlarmOn());

    }

    /**
     * Comprobar que la alarma se activa con presiones límite por encima y por debajo
     */
    @ParameterizedTest
    @ValueSource(doubles = {Alarm.HIGH_PRESSURE_THRESHOLD + 1,Alarm.LOW_PRESSURE_THRESHOLD - 1})
    public void testAlarmOnWithLimitsPressure(double preasure) {

        Sensor sensor = new Sensor();

        Alarm alarm = new Alarm(sensor);
        alarm.check(preasure);

        assertTrue(alarm.isAlarmOn());

    }

}
